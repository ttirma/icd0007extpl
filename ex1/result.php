<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Celsius to Fahrenheit Calculator</title>
</head>
<body>

<?php

    $input = $_GET['temp_in_celsius'];

    function toFahrenheit($temp) {
        return ($temp * 9 / 5) + 32;
    }

?>

<h3>Celsius to Fahrenheit Calculator</h3>

<strong>
    X degrees in Fahrenheit is Y degrees in Celsius
</strong>

<p>&copy; 2010-2019 </p>

</body>
</html>
